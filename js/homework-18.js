'use strict';
//Упражнение 1

let a = "100px";
let b = "323px";

let result = parseInt(a) + parseInt(b);

console.log(result);

//Упражнение 2

console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

//Упражнение 3

let d = 0.111;
console.log("d", Math.ceil(d));

let e = 45.333333;
console.log("e", Number(e.toFixed(1)));

let f = 3;
console.log("f", f ** 5);

let g = 4e14;
console.log("g", g);

let h = "1" !== 1;
console.log('h',h)

//Упражнение 4

console.log(0.1+0.2===0.3);// Вернёт false, почему?
//В JS 0.1 + 0.2 = не 0.3 а 0.30000000000000004 так как в JavaScript нет возможности для хранения точных значений дробей используя двоичную систему.
//Решается проблема с помощью округлений чисел

console.log(0.1 + 0.2)
console.log(Number((0.1 + 0.2).toFixed(1)))
console.log(+(0.1 + 0.2).toFixed(1))
