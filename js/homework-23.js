"use strict";

// Упражнение 1

function timerCall() {
  let number = Number(prompt("Введите число"));

  if (typeof number !== "number" || isNaN(number)) {
    return console.log("Не число");
  }
  if (number <= 0) {
    return console.log("Введите число больше 0");
  }
  const interval = setInterval(() => {
      if(number > 0) {
          console.log(`Осталось ${number}`)
          number--
      }
      else {
          clearInterval(interval)
          console.log('Время истекло')
      }
  }, 1000)
}

// timerCall()

// Упражнение 2

async function apiRequest() {
  const url = `https://reqres.in/api/users`;
  console.time("Запрос");
  const response = await fetch(url);

  if (!response.ok) {
    const message = `Ошибка: ${response.status}`;
    throw new Error(message);
  }

  const data = await response.json();
  console.timeEnd("Запрос");

  const users = [...data.data];
  console.log(`Получили пользователей: ${users.length}`);

  for (let user of users) {
    console.log(
        user.first_name,
        user.last_name,
        `(${user.email})`
        );
  }
}

apiRequest().catch((error) => console.log(error.message));
