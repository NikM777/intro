"use strict";

window.addEventListener("load", () => {
  inputName.value = localStorage.getItem("inputName");
  inputRating.value = localStorage.getItem("inputRating");
});

const inputName = document.querySelector(".add-review__name");
const inputRating = document.querySelector(".add-review__rating");
const errorNameMessage = document.querySelector(".error-name-message");
const errorRatingMessage = document.querySelector(".error-rating-message");
const sendButton = document.querySelector(".add-review__button");
const addReview = document.querySelector('.add-review')

class AddReviewForm {
  constructor(
    inputName,
    inputRating,
    sendButton,
    errorNameMessage,
    errorRatingMessage,
    addReview
    ) {
      this.inputName = inputName
      this.inputRating = inputRating
      this.sendButton = sendButton
      this.errorNameMessage = errorNameMessage
      this.errorRatingMessage = errorRatingMessage
      this.addReview = addReview

      this.inputName.addEventListener("input", () => {
        this.errorNameMessage.style.display = "none";
        localStorage.setItem("inputName", this.inputName.value);
      });
      this.inputRating.addEventListener("input", () => {
        this.errorRatingMessage.style.display = "none";
        localStorage.setItem("inputRating", this.inputRating.value);
      });
  }

  send() {
    sendButton.addEventListener("click", (event) => {
      event.preventDefault();
      if(this.checkValue()) {
          this.addReview.submit()
      }

    });
  }

  checkValue() {
    if (!this.inputName.value.length) {
      this.errorNameMessage.style.display = "block";
      this.errorNameMessage.textContent = "Вы забыли указать имя и фамилию";
      return;
    }
    if (this.inputName.value.length < 2) {
      this.errorNameMessage.style.display = "block";
      this.errorNameMessage.textContent = "Имя не может быть короче 2-х символов";
      return;
    }

    if (!this.inputRating.value.length) {
      this.errorRatingMessage.style.display = "block";
      this.errorRatingMessage.textContent = "Оценка должна быть от 1 до 5";
      return;
    }

    this.clearLocalStorage();
    return true
  }
  clearLocalStorage() {
    localStorage.removeItem("inputName");
    localStorage.removeItem("inputRating");
  }
}

const form = new AddReviewForm(
    inputName,
    inputRating,
    sendButton,
    errorNameMessage,
    errorRatingMessage,
    addReview
    );

form.send();

