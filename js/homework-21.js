'use strict';

//Упражнение 1

/**
 *@description Проверка объекта на наличие свойств
 *@param {object} obj
 *@returns {boolean} true если объект не имеет свойтсв, false если имеет
 */

const obj = {
    text: 'Hello'
}
const emptyObj = {}

function isEmpty(obj) {
   return !Object.keys(obj).length ? true : false
}

console.log(isEmpty(emptyObj))

//Упражнение 3

/**
 *@description Повышает в объекте зарплаты на определённый процент и записывает в переменную  сумму зарплат
 *@param {number} perzent
 *@returns {object} Возвращает объект с увеличенными зарплатами на определённый процент
 */

let salaries= {John:100000,Ann:160000,Pete:130000,}
let totalSalary = 0;

function raiseSalary(perzent) {
    for(let key in salaries) {
        if(salaries.hasOwnProperty(key)) {
            salaries[key] = Math.floor( (salaries[key] * perzent) / 100 + salaries[key])
            totalSalary += salaries[key]
        }
    }
    console.log(salaries)

    return salaries
}
raiseSalary(5)
console.log(totalSalary)
