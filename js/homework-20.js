'use strict';

// Упражнение 1

for (let i = 0; i <= 20; i++) {
    if(i % 2 === 0) {
        console.log(i)
    }
}

// Упражнение 2

function countTheNumbers () {
    let result = 0;
    let counter = 3;

    while(counter > 0) {
     let num = prompt('Введите число');
        if(/\S/.test(num) === false || num === null || Number.isNaN(Number(num))) {
            alert('Ошибка, вы ввели не число')
            break;
        } else {
            counter--;
            result += Number(num);
        }

    }
    console.log(result)

}

countTheNumbers()

// Упражнение 3

function getNameOfMonth(month) {
    const arr = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
    ]
    arr.forEach((item, index) => index === month ? console.log('Первый цикл', item) : false)
    arr.forEach((item, index) => index !== 9 ? console.log('Второй цикл',item) : false)

}
getNameOfMonth(0)

// Упражнение 4
//Это функция котороая выполняется сразу после объявления
//Она используется для создания закрытой области видимости, и применяется в паттерне «модуль».

// (function() {
//     console.log('IIFE')
// }())

