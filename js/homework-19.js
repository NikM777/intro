"use strict";
//Упражнение 1

let a = "$100";
let b = "300$";

let subA = Number(a.substring(1));
let subB = Number(b.substring(0, b.length - 1));

let summ = subA + subB;

console.log(summ);

//Упражнение 2

let message = " привет, медвед      ";

// let messageTrim = message.trim()
// let newMessage = messageTrim.charAt(0).toUpperCase() + messageTrim.slice(1)

let newMessage = message.trim().charAt(0).toUpperCase() + message.trim().slice(1);

console.log(message);
console.log(newMessage);

//Упражнение 3

function getAge() {
  let ageText;
  let age = prompt("Сколько вам лет");

  if (age === null || age === "") {
    alert("Не число");
    return getAge();
  }

  let lastTwoDigits = age[age.length - 2] + age[age.length - 1];

  if (lastTwoDigits > 10 && lastTwoDigits < 20) {
    ageText = "лет";
  } else {
    let lastDigit = age[age.length - 1];
    if (lastDigit == 1) ageText = "год";
    if (lastDigit > 1 && lastDigit < 5) ageText = "года";
    if (lastDigit == 0 || lastDigit > 4) ageText = "лет";
  }

  let answer = Number(age);

  if (Number.isNaN(answer) === true) {
    alert("Не число");
    return getAge();
  }

  if (answer < 0) {
    alert(`Введите положительное число`);
    return getAge();
  }

  if (Number.isInteger(answer) === false) {
    alert("Введите число без точки");
    return getAge();
  }

  if (answer >= 0 && answer < 4) return alert(`Вам ${answer} ${ageText} и вы младенец`);
  if (answer <= 11) return alert(`Вам ${answer} ${ageText} и вы ребёнок`);
  if (answer <= 18) return alert(`Вам ${answer} ${ageText} и вы подросток`);
  if (answer <= 40) return alert(`Вам ${answer} ${ageText} и вы познаёте жизнь`);
  if (answer <= 80) return alert(`Вам ${answer} ${ageText} и вы познали жизнь`);
  if (answer >= 81) return alert(`Вам ${answer} ${ageText} и вы долгожитель`);

}

getAge();


//Упражнение 4

function getWordCount () {
    let message='Я работаю со строками как профессионал!';
    let count = 1;
    let newMessage = message.trim().replace(/\s+/g, ' ');

    for(let elem of newMessage) {
      if(elem === ' ') {
        count += 1;
      }
    }

    console.log(newMessage)
    console.log(count)
}

getWordCount();

