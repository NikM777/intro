'use strict'

import { useRef } from "react"
import Sidebar from "../react-app/src/components/Sidebar/Sidebar"

window.addEventListener('load', () => {
    inputName.value = localStorage.getItem('inputName')
    inputRating.value = localStorage.getItem('inputRating')
    if(localStorage.getItem('goodsCounter')) {
        addToCart()
    }
})
const addReview = document.querySelector('.add-review')
const inputName = document.querySelector('.add-review__name')
const inputRating = document.querySelector('.add-review__rating')
const errorNameMessage = document.querySelector('.error-name-message')
const errorRatingMessage = document.querySelector('.error-rating-message')
const sendButton = document.querySelector('.add-review__button')

let goodsCounter = 0
const favIcon = document.querySelector('.header__fav-icon')
const addToCartButton = useRef()
console.log(addToCartButton)

addToCartButton.addEventListener('click', () => {
    if(goodsCounter > 0) {
        return removeFromCart()
     } else {
        return addToCart()
     }
})

function addToCart() {
    goodsCounter++
    localStorage.setItem('goodsCounter', goodsCounter)
    toggleCartClass()
    const favIconSpan = document.createElement('span')
    favIconSpan.classList.add('fav-icon-counter')
    favIconSpan.textContent = goodsCounter
    favIcon.appendChild(favIconSpan)
    }

function removeFromCart() {
    goodsCounter--
    localStorage.removeItem('goodsCounter')
    toggleCartClass()
    favIcon.removeChild(favIcon.lastChild)
}

function toggleCartClass() {
    addToCartButton.classList.toggle('product-card__button-inactive')
    ? addToCartButton.textContent = 'Товар уже в корзине'
    : addToCartButton.textContent = 'Добавить в корзину'
}

function checkValue ()  {
    if(!inputName.value.length) {
        errorNameMessage.style.display = 'block'
        errorNameMessage.textContent = 'Вы забыли указать имя и фамилию'
        return
    }
    if(inputName.value.length < 2) {
        errorNameMessage.style.display = 'block'
        errorNameMessage.textContent = 'Имя не может быть короче 2-хсимволов'
        return
    }

    if(!inputRating.value.length) {
        errorRatingMessage.style.display = 'block'
        errorRatingMessage.textContent = 'Оценка должна быть от 1 до 5'
        return
    }

    clearLocalStorage()
    return true
}

function clearLocalStorage() {
    localStorage.removeItem('inputName')
    localStorage.removeItem('inputRating')
}

inputName.addEventListener('input', () => {
    errorNameMessage.style.display = 'none'
    localStorage.setItem('inputName', inputName.value)
})
inputRating.addEventListener('input', () => {
    errorRatingMessage.style.display = 'none'
    localStorage.setItem('inputRating', inputRating.value)
})
sendButton.addEventListener('click', (btn) => {
    btn.preventDefault()
    if(checkValue()) {
        addReview.submit()
    }

} )






