import { configureStore } from "@reduxjs/toolkit";
import {
  persistStore,
  persistReducer,

} from "redux-persist";
import storage from "redux-persist/lib/storage";

import cartReducer from "../features/cart/cartSlice";

const persistConfig = {
  key: "root",
  storage,
};
const persistedReducer = persistReducer(persistConfig, cartReducer);

const logger = (store) => (next) => (action) => {
  console.log('action', action)
  const result = next(action)
  console.log('next state', store.getState())
  return result
}

let countAction = 0
const actionsCounter = store => next => action => {
  const result =  next(action)
  if(result.type !== 'persist/REHYDRATE' && result.type !== "persist/PERSIST") {
    countAction++
    console.log('Количество обработанных действий:', countAction)
    return  result
  }
}

export const store = configureStore({
  reducer: {
    cart: persistedReducer,
  },
  middleware: [logger,actionsCounter ]

});

export const persistor = persistStore(store);
