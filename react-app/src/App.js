import PageProduct from "./components/Pages/PageProduct/PageProduct";
import MainPage from "./components/Pages/MainPage/MainPage";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={"/"} element={<MainPage />} />
        <Route path={"/product"} element={<PageProduct />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
