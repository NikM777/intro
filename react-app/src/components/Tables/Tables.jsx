import React from "react";
import "./Tables.css";
function Tables() {
  return (
    <>
      <section className="tables">
        <table className="table">
        <caption className="table__title">Сравнение моделей</caption>
          <tbody>
            <tr className="table__row">
              <th className="table__subtitle">Модель</th>
              <th className="table__subtitle">Вес</th>
              <th className="table__subtitle">Высота</th>
              <th className="table__subtitle">Ширина</th>
              <th className="table__subtitle">Толщина</th>
              <th className="table__subtitle">Чип</th>
              <th className="table__subtitle">Объём памяти</th>
              <th className="table__subtitle">Аккумулятор</th>
            </tr>
            <tr className="table__row">
              <td className="table__data">Iphone 11</td>
              <td className="table__data">194 грамма</td>
              <td className="table__data">150.9 м.м</td>
              <td className="table__data">75.7 м.м</td>
              <td className="table__data">8.3 м.м</td>
              <td className="table__data">A13 Bionic chip</td>
              <td className="table__data">до 128 Гб</td>
              <td className="table__data">До 17 часов</td>
            </tr>
            <tr className="table__row">
              <td className="table__data">Iphone 12</td>
              <td className="table__data">164 грамма</td>
              <td className="table__data">146.7 м.м</td>
              <td className="table__data">71.5 м.м</td>
              <td className="table__data">7.4 м.м</td>
              <td className="table__data">A14 Bionic chip</td>
              <td className="table__data">до 256 Гб</td>
              <td className="table__data">До 19 часов</td>
            </tr>
            <tr className="table__row">
              <td className="table__data">Iphone 13</td>
              <td className="table__data">174 грамма</td>
              <td className="table__data">146.7 м.м</td>
              <td className="table__data">71.5 м.м</td>
              <td className="table__data">7.65 м.м</td>
              <td className="table__data">A15 Bionic chip</td>
              <td className="table__data">до 512 Гб</td>
              <td className="table__data">До 19 часов</td>
            </tr>
          </tbody>
        </table>
      </section>
    </>
  );
}

export default Tables;
