import React from "react";

import Header from '../../Header/Header'
import BreadCrumbs from "../../Header/BreadCrumbs/BreadCrumbs";
import ProductImages from "../../ProductImages/ProductImages";
import Configuration from "../../Configuration/Configuration";
import Specification from "../../Specification/Specification";
import Description from "../../Description/Description";
import Sidebar from "../../Sidebar/Sidebar";
import Reviews from "../../Reviews/Reviews";
import Tables from "../../Tables/Tables";
import Footer from "../../Footer/Footer";

import product from "../../../data";

import "./PageProduct.css";


function PageProduct() {
  const { memoryOptions, colorOptions } = product;

  return (
    <>
      <div className="container">
        <div className="wrapper">
          <Header  />
          <BreadCrumbs/>
          <ProductImages/>
          <main className="main">
            <div className="main__container">
              <div className="main__box">
                <Configuration
                  memoryOptions={memoryOptions}
                  colorOptions={colorOptions}
                />
                <Specification />
                <Description />
                <Tables />
              </div>
              <Sidebar />
            </div>
            <Reviews />
          </main>
        </div>
        <Footer />
      </div>
    </>
  );
}

export default PageProduct;
