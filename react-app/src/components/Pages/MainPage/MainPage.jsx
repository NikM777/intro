import React from "react";
import Footer from "../../Footer/Footer";
import Header from "../../Header/Header";
import "./MainPage.css";
function MainPage() {
  return (
    <>

        <div className="main-wrapper">
          <Header />
          <section className="main-page">
            <div className="main-page__container">
              <p className="main-page__description">
                Здесь должно быть содержимое главной страницы. Но в рамках курса
                главная страница используется лишь в демонстрационных целях
              </p>
              <a href={"product"} className="main-page__link link">
                Перейти на страницу товара
              </a>
            </div>

          </section>
        </div>
          <Footer />

    </>
  );
}

export default MainPage;
