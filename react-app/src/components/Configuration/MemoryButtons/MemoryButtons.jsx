import React from "react";

import "./MemoryButtons.css";

function MemoryButtons({
  memoryOptions,
  selectedMemoryBtn,
  handleClickMemoryBtn,
}) {
  return (
    <>
      <div className="configuration__memory">
        <h3 className="configuration__memory-title">
          Конфигурация памяти: {selectedMemoryBtn}
        </h3>
        <div className="configuration__memory-btns">
          {memoryOptions.map((memoryBtn) => (
            <button
              key={memoryBtn}
              className={
                memoryBtn === selectedMemoryBtn
                  ? "button_selected hover_button configuration__memory-btn"
                  : "hover_button configuration__memory-btn"
              }
              onClick={handleClickMemoryBtn}
            >
              {memoryBtn}
            </button>
          ))}
        </div>
      </div>
    </>
  );
}

export default MemoryButtons;
