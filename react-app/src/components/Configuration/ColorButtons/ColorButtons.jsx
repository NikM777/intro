import React from "react";
import "./ColorButtons.css";
import { colors } from "../../../assets/images";

function ColorButtons({ selectedColorBtn, handleClickColorBtn }) {
  return (
    <>
      <div className="configuration__color">
        <h3 className="configuration__color-title">
          Цвет товара: {selectedColorBtn}
        </h3>
        <div className="configuration__images">
          {colors.map((color) => (
            <img
              key={color.name}
              className={
                color.name === selectedColorBtn
                  ? "button_selected hover_button configuration__image"
                  : "hover_button configuration__image"
              }
              tabIndex="0"
              src={color.link}
              alt={color.name}
              name={color.name}
              onClick={handleClickColorBtn}
            />
          ))}
        </div>
      </div>
    </>
  );
}

export default ColorButtons;
