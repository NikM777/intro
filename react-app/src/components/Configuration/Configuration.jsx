import React, { useState } from "react";
import MemoryButtons from "./MemoryButtons/MemoryButtons";
import ColorButtons from "./ColorButtons/ColorButtons";

import "./Configuration.css";

function Configuration({ memoryOptions }) {
  const [selectedColorBtn, setSelectedColorBtn] = useState("Красный");
  const [selectedMemoryBtn, setSelectedMemoryBtn] = useState("128 ГБ");

  function handleClickColorBtn(e) {
    setSelectedColorBtn(e.target.name);
  }
  function handleClickMemoryBtn(e) {
    setSelectedMemoryBtn(e.target.textContent);
  }

  return (
    <>
      <section className="configuration">
        <ColorButtons
          selectedColorBtn={selectedColorBtn}
          handleClickColorBtn={handleClickColorBtn}
        />
        <MemoryButtons
          memoryOptions={memoryOptions}
          selectedMemoryBtn={selectedMemoryBtn}
          handleClickMemoryBtn={handleClickMemoryBtn}
        />
      </section>
    </>
  );
}

export default Configuration;
