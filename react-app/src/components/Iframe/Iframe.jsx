import React from "react";
import "./Iframe.css";

function Iframe() {
  return (
    <div className="iframe-container">
      <div className="iframe-box">
        <iframe
          className="iframe"
          sandbox="allow-same-origin"
          src="https://media.idownloadblog.com/wp-content/uploads/2022/05/Apple-advertisement-Privacy-iPhone-1500x1000.jpg"
        ></iframe>
        <iframe
          className="iframe"
          sandbox="allow-same-origin"
          src="https://media.idownloadblog.com/wp-content/uploads/2021/09/Apple-iPhone-13-Pro-advertisement.jpg"
        ></iframe>
      </div>
    </div>
  );
}

export default Iframe;
