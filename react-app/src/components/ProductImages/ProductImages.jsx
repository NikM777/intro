import React from "react";
import "./ProductImages.css";

import {
  iphoneFrontAndBackView,
  iphoneFrontView,
  iphoneSideView,
  iphoneBackCamera,
  iphoneBackAndFrontView,
} from "../../assets/images";

function ProductImages() {
  return (
    <>
      <section className="product-images">
        <div className="product-images">
          <h2 className="product-images__title">
            Смартфон Apple iPhone 13 Синий
          </h2>
          <div className="product-images__box">
            <img
              className="product-images__image"
              src={iphoneFrontAndBackView}
              alt="Iphone front and back view"
            />
            <img
              className="product-images__image"
              src={iphoneFrontView}
              alt="Iphone front view"
            />
            <img
              className="product-images__image"
              src={iphoneSideView}
              alt="Iphone side view"
            />
            <img
              className="product-images__image"
              src={iphoneBackCamera}
              alt="Iphone back camera"
            />
            <img
              className="product-images__image"
              src={iphoneBackAndFrontView}
              alt="Iphone back and front view"
            />
          </div>
        </div>
      </section>
    </>
  );
}

export default ProductImages;
