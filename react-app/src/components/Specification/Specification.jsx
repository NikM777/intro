import React from "react";
import './Specification.css'
function Specification() {
  return (
    <>
      <section className="specification">
        <h3 className="specification__title">Характеристики товара</h3>
        <ul className="specification__list">
          <li className="specification__item">
            Экран: <span className="specification__item-bold-text">6.1</span>
          </li>
          <li className="specification__item">
            Встроенная память: <span className="specification__item-bold-text">128 ГБ</span>
          </li>
          <li className="specification__item">
            Операционная система: <span className="specification__item-bold-text">iOS 15</span>
          </li>
          <li className="specification__item">
            Беспроводные интерфейсы: <span className="specification__item-bold-text">NFC, Bluetooth, Wi-Fi</span>
          </li>
          <li className="specification__item">
            Процессор:
            <a
              className="link specification__link"
              href="https://ru.wikipedia.org/wiki/Apple_A15"
              target="_blank"
              rel = "noreferrer"
            >
              Apple 15 Bionic
            </a>
          </li>
          <li className="specification__item">
            Вес: <span className="specification__item-bold-text">173 г</span>
          </li>
        </ul>
      </section>
    </>
  );
}

export default Specification;
