import React from "react";
import { useCurrentDate } from "@kundinos/react-hooks";
import "./Footer.css";
function Footer() {
  const currentDate = useCurrentDate();
  const fullYear = currentDate.getFullYear();

  return (
    <>
      <footer className="footer">
        <div className="footer__text-box">
          <h4 className="footer__title">
            © ООО «<span className="footer__orange-text">Мой</span>Маркет»,
            2018-{fullYear}.
          </h4>
          <span className="footer__text">
            Для уточнения информации звоните по номеру
            <a href="tel:+7 900 000 0000" className="link footer__link">
              {` 7 900 000 0000,`}
            </a>
            <br />а предложения по сотрудничеству отправляйте на почту
            <a
              href="mailto: partner@mymarket.com"
              className="link footer__link"
            >
             {` partner@mymarket.com`}
            </a>
          </span>
        </div>
        <div className="footer__up-link-box">
          <a className="link footer__up-link" href="#">
            Наверх
          </a>
        </div>
      </footer>
    </>
  );
}

export default Footer;
