import React from "react";
import styled from "styled-components";
import "./BreadCrumbs.css";

const BreadCrumbsNav = styled.nav`
  margin-bottom: 8px;
  display: flex;
  align-items: baseline;
  gap: 4px;
`;

function BreadCrumbs() {
  const items = [
    "Электроника",
    "Смартфоны и гаджеты",
    "Мобильные телефоны",
    "Apple",
  ];
  return (
    <>
      <BreadCrumbsNav>
        {items.map((link) => {
          return (
            <a key={link} href="#" className="link bread-crumbs__link">
              {link}
            </a>
          );
        })}
      </BreadCrumbsNav>
    </>
  );
}

export default BreadCrumbs;
