import React from "react";
import { useSelector } from "react-redux";
import "./Header.css";

import { logo } from "../../assets/images";

function Header() {
  const cartCounter = useSelector((state) => state.cart.products.length);
  const favoritesCounter = useSelector((state) => state.cart.favorites.length);

  return (
    <>
      <section className="header__title-box">
        <a className="header__icon" href="/">
          <img className="header__icon" src={logo} alt="shopping bag" />
        </a>

        <h1 className="header__title">
          <span className="header__title-span">Мой</span>Маркет
        </h1>
        <div className="header__icons-box">
          <div className="header__fav-icon">
            {!!favoritesCounter && (
              <span className="fav-icon-counter">{favoritesCounter}</span>
            )}
          </div>
          <div className="header__basket-icon">
            {!!cartCounter && (
              <span className="basket-icon-counter">{cartCounter}</span>
            )}
          </div>
        </div>
      </section>
    </>
  );
}

export default Header;
