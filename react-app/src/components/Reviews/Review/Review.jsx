import React from "react";
import { reviewsArray } from "../../../data.js";
import "./Review.css";

import { starIcon, greyStarIcon } from "../../../assets/images";

function Review() {
  const totalStars = 5;

  return (
    <>
      {reviewsArray.map((elem) => {
        return (
          <div key={elem.id} className="review">
            <img
              className="review__avatar"
              src={elem.photo}
              alt="Person photography"
            />
            <div className="review__box">
              <p className="review__name">{elem.name}</p>
              <div className="review__rating-box">
                {[...new Array(totalStars)].map((item, index) => {
                  return index < elem.activeStars
                    ? <img
                      key={index}
                      className="review__rating"
                      src={starIcon}
                      alt="5 star rating"
                    />
                   :
                    <img
                      key={index}
                      className="review__rating"
                      src={greyStarIcon}
                      alt="5 star rating"
                    />;
                })}
              </div>
              <div className="review__comment-container review-comment">
                <p className="review-comment__text">
                  <span className="review-comment__subtitle">
                    Опыт использования:
                  </span>
                  {elem.userExperience}
                </p>
                <p className="review-comment__text">
                  <span className="review-comment__subtitle">Достоинства:</span>{" "}
                  <br />
                  {elem.productAdvantages}
                </p>
                <p className="review-comment__text">
                  <span className="review-comment__subtitle">Недостатки:</span>{" "}
                  <br />
                  {elem.productFlaws}
                </p>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
}

export default Review;
