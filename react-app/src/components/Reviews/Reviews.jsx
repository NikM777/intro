import React from "react";
import AddReview from "./AddReview/AddReview";
import Review from "./Review/Review";

import "./Reviews.css";

function Reviews() {
  return (
    <>
      <section className="reviews">
        <div className="reviews-title__container">
          <div className="reviews-title__box">
            <h3 className="reviews__title">Отзывы</h3>
            <span className="reviews__counter">425</span>
          </div>
          <button className="reviews__btn hover_button">Добавить отзыв</button>
        </div>

        <div className="review__container">
          <Review />
        </div>
        <AddReview />
      </section>
    </>
  );
}

export default Reviews;
