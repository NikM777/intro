import React, { useState } from "react";
import "./AddReview.css";

function AddReview() {
  const [inputName, setInputName] = useState(() => {
    return localStorage.getItem('inputName') || ''
  });
  const [inputRating, setInputRating] = useState(() => {
    return localStorage.getItem('inputRating') || ''
  });
  const [isNameValid, setIsNameValid] = useState(true);
  const [isRatingValid, setIsRatingValid] = useState(true);
  const [isNameLengthValid, setIsNameLengthValid] = useState(true);

  const errors = {
    emptyNameError: (
      <span className="error-name-message">
        Вы забыли указать имя и фамилию
      </span>
    ),
    lengthNameError: (
      <span className="error-name-message">
        Имя не может быть короче 2-хсимволов
      </span>
    ),
    emptyRatingError: (
      <span className="error-rating-message">Оценка должна быть от 1 до 5</span>
    ),
  };

  function handleInputNameChange(e) {
    setIsNameValid(true);
    setIsNameLengthValid(true);
    setInputName(e.target.value);
    localStorage.setItem("inputName", e.target.value);
  }
  function handleInputRatingChange(e) {
    e.target.value = e.target.value
    .replace(/[^1-5.]/g, "")
    .replace(/(\..*)\./g, "$1");
    setIsRatingValid(true);
    setInputRating(e.target.value);
    localStorage.setItem("inputRating", e.target.value);
  }

  function checkValue() {
    if (!inputName) {
      return setIsNameValid(false);
    }
    if (inputName.length < 2) {
      return setIsNameLengthValid(false);
    }
    if (!inputRating) {
      return setIsRatingValid(false);
    }
    setIsNameValid(true);
    setIsRatingValid(true);
    clearLocalStorage();
    return true;
  }

  function clearLocalStorage() {
    localStorage.removeItem("inputName");
    localStorage.removeItem("inputRating");
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (checkValue()) {
      e.currentTarget.submit();
    }
  }

  return (
    <>
      <form onSubmit={handleSubmit} className="add-review">
        <fieldset className="add-review__container">
          <legend className="add-review__title">Добавить свой отзыв</legend>
          <div className="add-review-box">
            <div className="add-review-input-name">
              <input
                onChange={handleInputNameChange}
                value={inputName}
                className="add-review__name form-input"
                type="text"
                placeholder="Имя и фамилия"
              />
              {!isNameValid && errors.emptyNameError}
              {!isNameLengthValid && errors.lengthNameError}
            </div>

            <div className="add-review-input-rating">
              <input
                onChange={handleInputRatingChange}
                value={inputRating}
                className="add-review__rating form-input"
                type="text"
                placeholder="Оценка"
                maxLength="1"
              />
              {!isRatingValid && errors.emptyRatingError}
            </div>
          </div>
          <textarea
            name=""
            id=""
            className="add-review__comment form-input"
            type="text"
            placeholder="Текст отзыва"
          ></textarea>
          <button type="submit" className="add-review__button">
            Отправить отзыв
          </button>
        </fieldset>
      </form>
    </>
  );
}
export default AddReview;
