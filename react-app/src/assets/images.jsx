import logo from "./images/icons/main-icon.svg";
import starIcon from "./images/icons/star.svg";
import greyStarIcon from "./images/icons/graystar.svg";
import heartGrey  from './images/icons/heartgrey.svg'
import basketGrey  from './images/icons/basketgrey.svg'

import iphoneFrontAndBackView from "./images/image-1.webp";
import iphoneFrontView from "./images/image-2.webp";
import iphoneSideView from "./images/image-3.webp";
import iphoneBackCamera from "./images/image-4.webp";
import iphoneBackAndFrontView from "./images/image-5.webp";

import iphoneRed from "./images/color-1.webp";
import iphoneGreen from "./images/color-2.webp";
import iphonePink from "./images/color-3.webp";
import iphoneBlue from "./images/color-4.webp";
import iphoneWhite from "./images/color-5.webp";
import iphoneBlack from "./images/color-6.webp";

import firstPersonPhoto from "./images/review-1.jpeg";
import secondPersonPhoto from "./images/review-2.jpeg";

export {
  logo,
  starIcon,
  greyStarIcon,
  basketGrey,
  heartGrey,
  iphoneFrontAndBackView,
  iphoneFrontView,
  iphoneSideView,
  iphoneBackCamera,
  iphoneBackAndFrontView,
  iphoneRed,
  iphoneGreen,
  iphonePink,
  iphoneBlue,
  iphoneWhite,
  iphoneBlack,
  firstPersonPhoto,
  secondPersonPhoto,
};

export const colors = [
  { name: "Красный", link: iphoneRed },
  { name: "Зелёный", link: iphoneGreen },
  { name: "Розовый", link: iphonePink },
  { name: "Синий", link: iphoneBlue },
  { name: "Белый", link: iphoneWhite },
  { name: "Чёрный", link: iphoneBlack },
];
