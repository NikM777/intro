import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
  name: "cart",
  initialState: {
    products: [],
    favorites: [],
  },
  reducers: {
    addProduct: (state, action) => {
      return {
        ...state,
        products: [...state.products, action.payload],
      };
    },
    removeProduct: (state, action) => {
      const newState = state.products.filter(
        (product) => product.id !== action.payload.id
      );
      state.products = newState;
    },

    addToFavorites: (state, action) => {
      return {
        ...state,
        favorites: [...state.favorites, action.payload],
      };
    },

    removeFromFavorites: (state, action) => {
      const newState = state.favorites.filter(
        (favProduct) => favProduct.id !== action.payload.id
      );
      state.favorites = newState;
    },
  },
});

export const {
  addProduct,
  removeProduct,
  addToFavorites,
  removeFromFavorites,
} = cartSlice.actions;
export default cartSlice.reducer;
